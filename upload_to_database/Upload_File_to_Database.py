# This is a script that will upload a csv file to a postgres database


# Import the libraries
import pandas as pd
import numpy as np

import os
from tqdm import tqdm
import psycopg2
import sqlalchemy


# Load the database credentials
from connections.config import *


# A function to parse the credentials and create a sqlalchemy engine
def create_postgres_database_engine(database):
    
    params_alsd = config(section='ApplicationLayerSandbox')
    
    host = params_alsd['host']
    database = params_alsd['database']
    user = params_alsd['user']
    port = params_alsd['port']
    pw = params_alsd['password']
    return sqlalchemy.create_engine(f'postgresql://{user}:{pw}@{host}:{port}/{database}')

database_engine = create_postgres_database_engine('ApplicationLayerSandbox')


# Select a file to read
files_to_read = ['Pluto_Sample', 'PVA_Sample']

dataframe = pd.read_csv(f'files_to_upload/{files_to_read[1]}.csv')


# Choose the parameters for the upload

# The table name in the database
table_name_in_database = "Test"

# Do you want to replace the table if it exists or append to it?
replace_or_append_table = 'replace'

# Do you want the index column included in the upload?
include_table_index = False


# Upload the dataframe
dataframe.to_sql(table_name_in_database, database_engine, if_exists=replace_or_append_table, index=include_table_index, method = "multi" )


# Read the uploaded table
test_data_count = pd.read_sql(f'''SELECT COUNT(*) FROM "{table_name_in_database}";''', con = database_engine)

print(f"The number of rows uploaded is: {test_data_count['count'].iloc[0]}")